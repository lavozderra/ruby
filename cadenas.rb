=begin
    Cadenas con secuencias de escape -> separador =\s 
    salto de linea = \n
=end

puts 'Esto es una cadena de texto'
puts "Cadena concatenada con -> \s" + 'ssss'
puts "Cadena concatenada con -> \n" + 'ssss'
puts " Cadena concatenada con -> \r" + 'ssss'

# Mas metodos con Cadenas
cadena = 'Esto es una cadena'

puts cadena.reverse     # Devuelve la cadena escrita al reves
puts cadena.upcase      # Devuelve la cadena en mayusculas
puts cadena.downcase    # Devuelve la cadena en minusculas
puts cadena.swapcase    # Devuelve la cadena invertida entre mayusculas y minusculas
puts cadena.capitalize  # Devuelve la cadena con la primer letra en mayuscula
puts cadena.slice(0, 5)  # Devuelve la cadena en en partes de acuerdo al indice indicado en los parentesis

# Metodos Bang se representa con este signo --> !
# son metodos peligrosos
cadena = 'Esto es una cadena'
res = cadena.upcase!
puts res

