# Rangos se usan mediante 2 puntos .. y con to_a los convierte en un arreglo
# aceptan numeros enteros y negativos

puts (1..20).to_a

 # Métodos de Rangos
 rango = (1..20)

 puts rango.min # Devuelve el numero minimo de rango
 puts rango.max # Devuelve el numero maximo de rango
 puts rango.include?(11) # Pregunta si el numero pasado por parametro viene dentro de rango, si existe devuelve true sino false
 
 # ↓ Comparacion logica, devuelve el mismo resultado que include?(11)
 # pero mas simplificado
 puts (2..22) === 5 