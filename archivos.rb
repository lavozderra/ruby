# Archivos en Ruby
# Asi creamos archivos en Ruby con la clase File y el metodo open
File.open("time.rb", "w") do |texto|
    texto.puts '# Con la clase ime obtenemos el tiempo del sistema'
    # texto.puts '# Lenguaje Ruby POO'
end

# Asi leemos archivos en Ruby con la clase File y el metodo open
File.open("poo.md", "r") do |text|
    # Utilizamos un ciclo while para recorrer cada linea escrita en el archivo
    # y el metodo gets
    while linea = text.gets
        puts linea
    end
end
